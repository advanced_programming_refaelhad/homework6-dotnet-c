﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;


namespace Calendar
{
    public partial class Form2 : Form
    {
        

        public Form2()
        {
            InitializeComponent();//init - all the start msgs to user
            label1.Text = "שם משתמש";
            label2.Text = "סיסמא";
            label3.Text = "שם משתמש";
            label4.Text = "סיסמא";
            makelogin.Text = "בצע כניסה";
            signup.Text = "אין לך חשבון ? בבקשה הירשם";
            signupb.Text = "הירשם";
            showpasswoed.PasswordChar = '*';//to hide the input pass
            boxpass.PasswordChar = '*';//to hide the input pass
            ifgoodlogin.Text = "";
           
        }

        private void makelogin_Click(object sender, EventArgs e)//login tap
        {

            //-------------------------this is take all the file and trim a empty lines---------
            string[] text = File.ReadAllLines("Users.txt").Where(s => s.Trim() != string.Empty).ToArray();
            File.Delete("Users.txt");
            File.WriteAllLines("Users.txt", text);
            //----------------------------------------------------------------------------------

            int index;//to index of (',') 

            string[] tmp;//to save the splite after he destroyed
            string splitname;//to hold the splite namme
            string splitpass;//hold the splite pass
            string username = showusername.Text;//take the username that user input (text box)
            string password = showpasswoed.Text;//take the password that user input (text box)
            string[] lines = System.IO.File.ReadAllLines("Users.txt");//read all the lines of users to lines (char** - string[])



            
                foreach (string s in lines)//for all line in lines (s = line in lines)
                {
                    index = s.IndexOf(',');//index of (',')
                    tmp = s.Split(',');//tmp = s splite

                    splitname = tmp[0];//name = tmp in first part

                    splitpass = s.Remove(0, index + 1);//remove all the part by far the (',') and put the output to splitpass
                   
                      if (splitname == username && splitpass == password)//if in one line the username = to input and like that the pass so good
                     {
                        ifgoodlogin.ForeColor = System.Drawing.Color.Green;//make the font green
                        ifgoodlogin.Text = "התחברת בהצלחה";//print to laybel
                        ifgoodlogin.Refresh();//refrash for the sleep
                        System.Threading.Thread.Sleep(500);//sleep for you can see the msg and for the buty


                        Form3 form3 = new Form3(username);//open new form - calender form and send him your name
                    
                        form3.Show();//show him
                        this.Hide();//hide this 


                        break;
                     }
                    else // if no good its print not good
                     {
                             ifgoodlogin.ForeColor = System.Drawing.Color.Red;//make the font red
                             ifgoodlogin.Text = "פרטי ההתחברות שגויים";
                             MessageBox.Show("!פרטי ההתחברת שגויים", "שגיאה", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            //אלכס כאן המסג בוקס מופיע פעמיים וניסיתי לבדוק למה אבל לא הצלחתי לתקן
                     }


                }

            }


            

        

        private void quit_Click(object sender, EventArgs e)//exit click
        {

           Application.Exit();
        }




        private void signupb_Click(object sender, EventArgs e)//for the sigup
        {

            string sigupuser = boxuser.Text;//hold the user in string
            string sigupass = boxpass.Text;//כנ"ל


            if (sigupuser.Length != 0 && sigupass.Length != 0)//if them both (them len) not 0
            {
                using (StreamWriter w = File.AppendText("Users.txt"))
                {
                    w.WriteLine(sigupuser + "," + sigupass);//add username and pass line to users.txt
                }

                
                sigupoutput.ForeColor = System.Drawing.Color.Green;//make the font green
                sigupoutput.Text = "!נרשמת בהצלחה";//print this msg

                //-------------------------this is take all the file and trim a empty lines---------
                string[] text = File.ReadAllLines("Users.txt").Where(s => s.Trim() != string.Empty).ToArray();
                File.Delete("Users.txt");
                File.WriteAllLines("Users.txt", text);
                //-----------------------------------------------------------------------------------
            }
            else//if not good
            {


                sigupoutput.ForeColor = System.Drawing.Color.Red;//make the font red
                sigupoutput.Text = "לא הצלחת להירשם";//print this msg


            }








        }

       

    }
}
