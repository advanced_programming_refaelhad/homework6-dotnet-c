﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Calendar
{
    public partial class Form3 : Form
    {
        public string username;//make this global
        string[] lines;//to read all the lines to lines verb
        static bool iffound = false;//a bool to know if found or not
        
        string path;//path - global to get fom all the classes
        public Form3(string name)
        {

            InitializeComponent();
            allprint.Text = "";
            //////////////////////////

            hello.Text = name + " שלום";//hello msg
            path = name + "BD.txt";//"משרשר" - the name + BD.TXT
            username = name;//make the name global wite username

            //if exist so good we close him and continue - if not this creat him and continue
            FileStream fs = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
            //close him
            fs.Close();

            //-------------------------this is take all the file and trim a empty lines---------
            string[] text = File.ReadAllLines(path).Where(s => s.Trim() != string.Empty).ToArray();
            File.Delete(path);
            File.WriteAllLines(path, text);
            //----------------------------------------------------------------------------------

            lines = System.IO.File.ReadAllLines(path);//get all the lines on file to lines



        }

        private void button1_Click(object sender, EventArgs e)
        {
            //-------------------------this is take all the file and trim a empty lines---------
            string[] text = File.ReadAllLines(path).Where(s => s.Trim() != string.Empty).ToArray();
            File.Delete(path);
            File.WriteAllLines(path, text);
            //------------------------------------------------------------------------------------

            lines = System.IO.File.ReadAllLines(path);//read all the lines to lines (i do it again every time in the start of the event)
            iffound = false;//make him false
            
            int index;//index of , 
            string[] tmp;//for him dont destroyd
            
            string splitname;
            string splitbirt;

            if (lines.Length != 0)//if there is lines at all
            {
                //this the choice of the user over and over again
                string choicebirt = monthCalendar1.SelectionStart.Month.ToString() + "/" + monthCalendar1.SelectionStart.Day.ToString() + "/" + monthCalendar1.SelectionStart.Year.ToString();

                
                    foreach (string s in lines)//for line in lines 
                    {
                        index = s.IndexOf(',');
                        tmp = s.Split(',');

                        splitname = tmp[0];//split name
                        splitbirt = s.Remove(0, index + 1);//split birth
                        
                        if (splitbirt == choicebirt)//if the splite birt = to choice so we print the name - how selebraite
                    {

                        outputifbirt.ForeColor = System.Drawing.Color.Green;//make the font green
                        outputifbirt.Text = "חוגג יום הולדת " + splitname + " , " + splitbirt + " בתאריך ";//print
                        iffound = true;//for we can know how selebraite
                        break;
                    }



                    }
                    if (iffound == false)//if we dont found so we print this msg
                    {
                    outputifbirt.ForeColor = System.Drawing.Color.Red;//make font red
                    outputifbirt.Text = "לא נמצא יום הולדת בתאריך זה";//print msg
                     }
                   
                }
            else
            {
                outputifbirt.ForeColor = System.Drawing.Color.Red;//make font red
                outputifbirt.Text = "לא הוגדרו ימי הולדת כלל למשתמש זה";//if no lines at all
            }

        }



        private void button2_Click(object sender, EventArgs e)//exit click button
        {
            Application.Exit();
        }


        private void button1_Click_1(object sender, EventArgs e)//if this button - print all the birtday
        {
            //-------------------------this is take all the file and trim a empty lines---------
            string[] text = File.ReadAllLines(path).Where(s => s.Trim() != string.Empty).ToArray();
            File.Delete(path);
            File.WriteAllLines(path, text);
            //------------------------------------------------------------------------------------

            lines = System.IO.File.ReadAllLines(path);//all the lines to lines

            allprint.Text = "";

           

            int index;//index of ,
            string[] tmp;

            string splitname;
            string splitbirt;

            if (lines.Length != 0)//if lines not 0
            {
                 allprint.ForeColor = System.Drawing.Color.Green;//green
                    foreach (string s in lines)//for all line
                    {
                        index = s.IndexOf(',');
                        tmp = s.Split(',');
                        splitname = tmp[0];//split name
                        splitbirt = s.Remove(0, index + 1);//split birth

                        //print the birth + name and make new line
                        allprint.Text += "חוגג יום הולדת" + splitname + "," + "בתאריך"+splitbirt;
                        allprint.Text += "\n";
                       
                        

                    }
                    
                }
                else//if 0 in lines
                {

                    allprint.ForeColor = System.Drawing.Color.Red;//red the msg
                    allprint.Text = "לא הוגדרו ימי הולדת כלל למשתמש זה";


                }






            }



        private void button3_Click(object sender, EventArgs e)//if add button was click
        {
            Form form4 = new Form4(username);//make form4 - add birthday
            form4.Show();//show him
            
        }
    }
}
