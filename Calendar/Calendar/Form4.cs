﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace Calendar
{
    public partial class Form4 : Form
    {

        string[] lines;//lines of file
        string path;//path file
        public Form4(string name)
        {

            InitializeComponent();

            path = name + "BD.txt";

            //-------------------------this is take all the file and trim a empty lines---------
            string[] text = File.ReadAllLines(path).Where(s => s.Trim() != string.Empty).ToArray();
            File.Delete(path);
            File.WriteAllLines(path, text);
            //------------------------------------------------------------------------------------

            lines = System.IO.File.ReadAllLines(path);//lines of reading filetxt

        }

        private void add_Click(object sender, EventArgs e)
        {

            //the user choice
            string choicebirt = monthCalendar1.SelectionStart.Month.ToString() + "/" + monthCalendar1.SelectionStart.Day.ToString() + "/" + monthCalendar1.SelectionStart.Year.ToString();

                
                int index;//index of ,
                string[] tmp;

                string splitname;
                string splitbirt;

                bool iffound = false;//if found
                
                foreach (string s in lines)
                {
                    index = s.IndexOf(',');
                    tmp = s.Split(',');
                    splitname = tmp[0];//splite the name
                    splitbirt = s.Remove(0, index + 1);//split the pass
                    
                    if (splitbirt == choicebirt)//if was a match
                    {
                        iffound = true;//iffound is true now
                    }

                }


            if (iffound == false)//if false - good we can add
            {
                ifaddgood.ForeColor = System.Drawing.Color.Green;//green
                
               using (StreamWriter sw = File.AppendText(path)) //add the line to the file
                {
                    sw.WriteLine("\n"+name.Text + "," + choicebirt);
                   // new line + the name + , + the birth
                }	


              

                ifaddgood.Text = "! יום ההולדת נוסף בהצלחה";//print 
                ifaddgood.Refresh();//refrash for the sleep
                System.Threading.Thread.Sleep(400);//for the buty
                this.Close();//close this
               
            }
            else//if match - there is 2 times 
            {
                ifaddgood.ForeColor = System.Drawing.Color.Red;//red
                ifaddgood.Text = "שגיאה - יום הולדת כפול";//print error
                ifaddgood.Refresh();///refres for the sleep
                System.Threading.Thread.Sleep(400);//for the buty
                this.Close();//close


            }

            //-------------------------this is take all the file and trim a empty lines---------
            string[] text = File.ReadAllLines(path).Where(s => s.Trim() != string.Empty).ToArray();
            File.Delete(path);
            File.WriteAllLines(path, text);
            //------------------------------------------------------------------------------------
             
        }

        
    }
}
