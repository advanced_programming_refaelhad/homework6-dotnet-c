﻿namespace Calendar
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.showusername = new System.Windows.Forms.TextBox();
            this.showpasswoed = new System.Windows.Forms.TextBox();
            this.signupb = new System.Windows.Forms.Button();
            this.signup = new System.Windows.Forms.Label();
            this.quit = new System.Windows.Forms.Button();
            this.ifgoodlogin = new System.Windows.Forms.Label();
            this.boxpass = new System.Windows.Forms.TextBox();
            this.boxuser = new System.Windows.Forms.TextBox();
            this.sigupoutput = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.makelogin = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // showusername
            // 
            this.showusername.Location = new System.Drawing.Point(15, 91);
            this.showusername.Name = "showusername";
            this.showusername.Size = new System.Drawing.Size(109, 20);
            this.showusername.TabIndex = 0;
            // 
            // showpasswoed
            // 
            this.showpasswoed.Location = new System.Drawing.Point(15, 135);
            this.showpasswoed.Name = "showpasswoed";
            this.showpasswoed.Size = new System.Drawing.Size(109, 20);
            this.showpasswoed.TabIndex = 3;
            // 
            // signupb
            // 
            this.signupb.Location = new System.Drawing.Point(356, 186);
            this.signupb.Name = "signupb";
            this.signupb.Size = new System.Drawing.Size(214, 23);
            this.signupb.TabIndex = 5;
            this.signupb.Text = "sign up";
            this.signupb.UseVisualStyleBackColor = true;
            this.signupb.Click += new System.EventHandler(this.signupb_Click);
            // 
            // signup
            // 
            this.signup.AutoSize = true;
            this.signup.Font = new System.Drawing.Font("Mistral", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signup.Location = new System.Drawing.Point(387, 53);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(33, 13);
            this.signup.TabIndex = 6;
            this.signup.Text = "label1";
            // 
            // quit
            // 
            this.quit.Location = new System.Drawing.Point(145, 186);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(75, 23);
            this.quit.TabIndex = 7;
            this.quit.Text = "יציאה";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // ifgoodlogin
            // 
            this.ifgoodlogin.AutoSize = true;
            this.ifgoodlogin.Location = new System.Drawing.Point(12, 241);
            this.ifgoodlogin.Name = "ifgoodlogin";
            this.ifgoodlogin.Size = new System.Drawing.Size(35, 13);
            this.ifgoodlogin.TabIndex = 8;
            this.ifgoodlogin.Text = "label1";
            // 
            // boxpass
            // 
            this.boxpass.Location = new System.Drawing.Point(356, 135);
            this.boxpass.Name = "boxpass";
            this.boxpass.Size = new System.Drawing.Size(100, 20);
            this.boxpass.TabIndex = 11;
            // 
            // boxuser
            // 
            this.boxuser.Location = new System.Drawing.Point(356, 91);
            this.boxuser.Name = "boxuser";
            this.boxuser.Size = new System.Drawing.Size(100, 20);
            this.boxuser.TabIndex = 12;
            // 
            // sigupoutput
            // 
            this.sigupoutput.AutoSize = true;
            this.sigupoutput.Location = new System.Drawing.Point(363, 251);
            this.sigupoutput.Name = "sigupoutput";
            this.sigupoutput.Size = new System.Drawing.Size(0, 13);
            this.sigupoutput.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ravie", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(145, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 21);
            this.label1.TabIndex = 14;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ravie", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(141, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 21);
            this.label2.TabIndex = 15;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ravie", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(494, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(76, 21);
            this.label3.TabIndex = 16;
            this.label3.Text = "label3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ravie", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(492, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 21);
            this.label4.TabIndex = 17;
            this.label4.Text = "label4";
            // 
            // makelogin
            // 
            this.makelogin.Image = global::Calendar.Properties.Resources.ok_2_icon__1_;
            this.makelogin.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.makelogin.Location = new System.Drawing.Point(12, 186);
            this.makelogin.Name = "makelogin";
            this.makelogin.Size = new System.Drawing.Size(112, 23);
            this.makelogin.TabIndex = 4;
            this.makelogin.Text = "login";
            this.makelogin.UseVisualStyleBackColor = true;
            this.makelogin.Click += new System.EventHandler(this.makelogin_Click);
            // 
            // Form2
            // 
            this.AccessibleDescription = "v";
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(654, 301);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.sigupoutput);
            this.Controls.Add(this.boxuser);
            this.Controls.Add(this.boxpass);
            this.Controls.Add(this.ifgoodlogin);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.signup);
            this.Controls.Add(this.signupb);
            this.Controls.Add(this.makelogin);
            this.Controls.Add(this.showpasswoed);
            this.Controls.Add(this.showusername);
            this.Name = "Form2";
            this.Text = "מסך ההתחברות";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox showusername;
        private System.Windows.Forms.TextBox showpasswoed;
        private System.Windows.Forms.Button makelogin;
        private System.Windows.Forms.Button signupb;
        private System.Windows.Forms.Label signup;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Label ifgoodlogin;
        private System.Windows.Forms.TextBox boxpass;
        private System.Windows.Forms.TextBox boxuser;
        private System.Windows.Forms.Label sigupoutput;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;

    }
}